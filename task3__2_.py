# Ввод координат векторов и скаляра
x = []
y = []
for i in ('x', 'y', 'z'):
    print(f'Введите координату {i} вектора x')
    x_var = int(input())
    x.append(x_var)
for i in ('x', 'y', 'z'):
    print(f'Введите координату {i} вектора y')
    y_var = int(input())
    y.append(y_var)
a = int(input('Введите скаляр a\n'))


# Вычисление координат искомого вектора
def calculate(x, y, a):
    z = []
    zx = x[0] * y[0] * a
    zy = x[1] * y[1] * a
    zz = x[2] * y[2] * a
    z.append(zx)
    z.append(zy)
    z.append(zz)
    return z
# Вывод результата
print('Вектор z - ', calculate(x, y, a))
